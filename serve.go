package main

import (
	"crypto/md5"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/fanyang01/stc-server/database"
)

// Tx is the context of handler function
type Tx struct {
	token *jwt.Token
}

// Verified handler function
type vHandlerFunc func(http.ResponseWriter, *http.Request, *Tx)

func init() {
	b, err := ioutil.ReadFile(*signKeyFile)
	fatal(err)
	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(b)
	fatal(err)

	b, err = ioutil.ReadFile(*verifyKeyFile)
	fatal(err)
	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(b)
	fatal(err)
}

func serve() {
	http.HandleFunc("/api/auth", method(authHandler, "POST"))
	http.HandleFunc("/api/account", method(verify(accountHandler), "GET"))
	http.HandleFunc("/api/stock", method(verify(stockHandler), "GET"))
	http.HandleFunc("/api/stock/data", method(verify(stockDataHandler), "GET"))
	http.HandleFunc("/api/industry", method(verify(industryHandler), "GET"))
	http.HandleFunc("/api/history", method(verify(historyHandler), "GET"))
	http.HandleFunc("/api/trading", method(verify(tradingHandler), "POST"))
	http.HandleFunc("/api/password", method(verify(passwordHandler), "POST"))
	http.Handle("/", http.FileServer(http.Dir("./frontend")))

	addr := *host + ":" + *port
	log.Printf("Serve on address %s...\n", addr)
	// For debug
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Fatal(http.ListenAndServe(addr, nil))
}

func bindJSON(rw http.ResponseWriter, req *http.Request, v interface{}) bool {
	decoder := json.NewDecoder(req.Body)
	err := decoder.Decode(v)
	if err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		log.Printf("error in decoding request body: %v\n", err)
		return false
	}
	return true
}

func writeJSON(rw http.ResponseWriter, v interface{}) bool {
	encoder := json.NewEncoder(rw)
	err := encoder.Encode(v)
	if err != nil {
		log.Printf("error in encoding json to response: %v\n", err)
		return false
	}
	return true
}

func method(fn http.HandlerFunc, method string) http.HandlerFunc {
	return func(rw http.ResponseWriter, req *http.Request) {
		if req.Method != method {
			http.Error(rw, "Invalid Method", http.StatusMethodNotAllowed)
			return
		}
		fn(rw, req)
	}
}

// verify verifies the token header of requests
func verify(fn vHandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, req *http.Request) {
		accessToken := req.Header.Get("AccessToken")
		if accessToken == "" {
			rw.WriteHeader(http.StatusUnauthorized)
			return
		}
		token, err := jwt.Parse(accessToken, func(t *jwt.Token) (interface{}, error) {
			if _, ok := t.Method.(*jwt.SigningMethodRSA); !ok {
				return nil, fmt.Errorf("unexpected signing method: %v", t.Header["alg"])
			}
			return verifyKey, nil
		})
		switch err.(type) {
		case nil:
			if !token.Valid {
				rw.WriteHeader(http.StatusUnauthorized)
				return
			}
		case *jwt.ValidationError:
			vErr := err.(*jwt.ValidationError)
			switch vErr.Errors {
			case jwt.ValidationErrorExpired:
				http.Error(rw, "token expired", http.StatusUnauthorized)
				return
			default:
				rw.WriteHeader(http.StatusInternalServerError)
				log.Printf("token validation error: %v\n", vErr.Errors)
				return
			}
		default:
			rw.WriteHeader(http.StatusInternalServerError)
			log.Printf("token parse error: %v\n", err)
			return
		}
		fn(rw, req, &Tx{
			token: token,
		})
	}
}

func authHandler(rw http.ResponseWriter, req *http.Request) {
	var login struct {
		ID       string `json:"id"`
		Password string `json:"password"`
	}
	if ok := bindJSON(rw, req, &login); !ok {
		return
	}

	pw, err := DB.QueryPassword(login.ID)
	switch {
	case err == sql.ErrNoRows:
		http.Error(rw, "none existed ID", http.StatusBadRequest)
		return
	case err != nil:
		log.Println(err)
		rw.WriteHeader(http.StatusInternalServerError)
		return
	default:
		if pw != fmt.Sprintf("%x", md5.Sum([]byte(login.Password))) {
			http.Error(rw, "password is incorrect", http.StatusUnauthorized)
			return
		}
	}

	t := jwt.New(jwt.GetSigningMethod("RS256"))
	t.Claims["UID"] = login.ID
	t.Claims["exp"] = time.Now().Add(tokenExpireTime).Unix()
	tokenString, err := t.SignedString(signKey)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		log.Fatalf("error in signing token: %v\n", err)
		return
	}
	writeJSON(rw, struct {
		AccessToken string
	}{
		AccessToken: tokenString,
	})
}

func accountHandler(rw http.ResponseWriter, req *http.Request, tx *Tx) {
	id := req.FormValue("accountID")
	if id != tx.token.Claims["UID"].(string) {
		// Attempt to access others' information
		rw.WriteHeader(http.StatusForbidden)
		return
	}
	account, err := DB.QueryAccount(id)
	if ok := checkDBError(rw, err, "none existed account"); !ok {
		return
	}
	writeJSON(rw, account)
}

func stockHandler(rw http.ResponseWriter, req *http.Request, tx *Tx) {
	id := req.FormValue("stockID")
	if id == "" {
		rw.WriteHeader(http.StatusBadRequest)
		return
	}
	stock, err := DB.QueryStock(id)
	if ok := checkDBError(rw, err, "none existed stock"); !ok {
		return
	}
	writeJSON(rw, stock)
}

func stockDataHandler(rw http.ResponseWriter, req *http.Request, tx *Tx) {
	id := req.FormValue("stockID")
	if id == "" {
		rw.WriteHeader(http.StatusBadRequest)
		return
	}
	stockInstant, err := DB.QueryStockInstant(id)
	if ok := checkDBError(rw, err, "stock instant data is not available"); !ok {
		return
	}

	priceRange, err := DB.QueryPriceRange(id)
	if ok := checkDBError(rw, err, "stock price range is not available"); !ok {
		return
	}

	writeJSON(rw, struct {
		*database.StockInstant
		*database.PriceRange
	}{
		StockInstant: stockInstant,
		PriceRange:   priceRange,
	})
}

func industryHandler(rw http.ResponseWriter, req *http.Request, tx *Tx) {
	id := req.FormValue("industryID")
	if id == "" {
		rw.WriteHeader(http.StatusBadRequest)
		return
	}
	industry, err := DB.QueryIndustry(id)
	if ok := checkDBError(rw, err, "industry information is not available"); !ok {
		return
	}
	writeJSON(rw, industry)
}

func historyHandler(rw http.ResponseWriter, req *http.Request, tx *Tx) {
	id := req.FormValue("accountID")
	if id != tx.token.Claims["UID"].(string) {
		// Attempt to access others' information
		rw.WriteHeader(http.StatusForbidden)
		return
	}
	instructions, err := DB.QueryHistory(id)
	if ok := checkDBError(rw, err, "none existed id"); !ok {
		return
	}
	writeJSON(rw, instructions)
}

func tradingHandler(rw http.ResponseWriter, req *http.Request, tx *Tx) {
	var tr tradingReq
	if ok := bindJSON(rw, req, &tr); !ok {
		return
	}
	if tr.AccountID != tx.token.Claims["UID"].(string) {
		rw.WriteHeader(http.StatusForbidden)
		return
	}
	if err := submitTradingReq(tr); err != nil {
		log.Println(err)
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}
	rw.WriteHeader(http.StatusOK)
}

func passwordHandler(rw http.ResponseWriter, req *http.Request, tx *Tx) {
	var pw struct {
		ID               string `json:"id"`
		OldPassword      string `json:"oldPassword"`
		NewPassword      string `json:"newPassword"`
		NewPasswordAgain string `json:"newPasswordAgain"`
	}
	if ok := bindJSON(rw, req, &pw); !ok {
		return
	}
	if pw.ID != tx.token.Claims["UID"].(string) {
		rw.WriteHeader(http.StatusForbidden)
		return
	}
	password, err := DB.QueryPassword(pw.ID)
	switch {
	case err == sql.ErrNoRows:
		http.Error(rw, "none existed ID", http.StatusBadRequest)
		return
	case err != nil:
		log.Println(err)
		rw.WriteHeader(http.StatusInternalServerError)
		return
	default:
		if fmt.Sprintf("%x", md5.Sum([]byte(pw.OldPassword))) != password {
			http.Error(rw, "password is incorrect", http.StatusUnauthorized)
			return
		}
	}
	if pw.NewPassword == "" {
		http.Error(rw, "invalid new password", http.StatusNotAcceptable)
		return
	}
	if pw.NewPassword != pw.NewPasswordAgain {
		http.Error(rw, "new password is inconsistent", http.StatusNotAcceptable)
		return
	}
	// Pass check
	err = DB.UpdatePassword(fmt.Sprintf("%x", md5.Sum([]byte(pw.NewPassword))), pw.ID)
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return
	}
	rw.WriteHeader(http.StatusOK)
}

// checkDBError checks err if it is sql.ErrNoRows
func checkDBError(rw http.ResponseWriter, err error, msg string) bool {
	switch {
	case err == sql.ErrNoRows:
		http.Error(rw, msg, http.StatusBadRequest)
		return false
	case err != nil:
		rw.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return false
	default:
		return true
	}
}
