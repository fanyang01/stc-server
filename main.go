package main

import (
	"crypto/rsa"
	"database/sql"
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"runtime"
	"time"

	"github.com/fanyang01/stc-server/database"
	_ "github.com/go-sql-driver/mysql"
)

type dbConfig struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	Name     string `json:"dbname"`
}

var (
	ncpu         = flag.Int("cpu", 2, "number of CPUs to use")
	dbConfigFile = flag.String("db-config", "db.json", "database config file")
	host         = flag.String("host", "localhost", "host name/address to serve on")
	port         = flag.String("port", "9000", "port listen to")
	tradingAPI   = flag.String("trading-api", "http://localhost:8080/api.php", "central trading system API address")

	signKeyFile     = flag.String("sign-key", "app.rsa", "private key for sign token")
	verifyKeyFile   = flag.String("verify-key", "app.rsa.pub", "public key for verify token")
	signKey         *rsa.PrivateKey
	verifyKey       *rsa.PublicKey
	tokenExpireTime = time.Minute * 30

	DB *database.DB
)

func main() {
	flag.Parse()
	runtime.GOMAXPROCS(*ncpu)

	addr := dbDSN()
	log.Printf("Connect to database: %s...\n", addr)
	addr += "?parseTime=true"
	db, err := sql.Open("mysql", addr)
	fatal(err)

	DB, err = database.New(db)
	fatal(err)
	serve()
}

func dbDSN() string {
	s, err := ioutil.ReadFile(*dbConfigFile)
	fatal(err)
	var c dbConfig
	err = json.Unmarshal(s, &c)
	fatal(err)

	var dsn string
	if c.User != "" {
		dsn += c.User
		if c.Password != "" {
			dsn += ":" + c.Password
		}
		dsn += "@"
	}
	dsn += "tcp"
	if c.Host != "" {
		dsn += "(" + c.Host
		if c.Port != "" {
			dsn += ":" + c.Port
		}
		dsn += ")"
	}
	if c.Name == "" {
		log.Fatalf("parsing config file %s: please specify a database name\n", *dbConfigFile)
	}
	dsn += "/" + c.Name
	return dsn
}

func fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
