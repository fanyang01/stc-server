# 股票交易客户端

## 源码组织形式

	.
	├── app.rsa
	├── app.rsa.pub
	├── client.go
	├── database
	│   ├── db.go
	│   ├── def.go
	│   ├── query.go
	│   └── update.go
	├── frontend
	│   ├── account-view.html
	│   ├── ajax-behavior.html
	│   ├── app.css
	│   ├── app.js
	│   ├── bower_components
	│   ├── bower.json
	│   ├── buy-sell-view-behavior.html
	│   ├── buy-view.html
	│   ├── change-password.html
	│   ├── elements.html
	│   ├── fmt-behavior.html
	│   ├── index.html
	│   ├── login-view.html
	│   ├── logout-view.html
	│   ├── main-app.html
	│   ├── main-view.html
	│   ├── more-table.html
	│   ├── query-view.html
	│   ├── README.md
	│   ├── routes.html
	│   ├── sell-view.html
	│   ├── stock-card.html
	│   ├── stock-details.html
	│   ├── toggle-main-drawer-behavior.html
	│   └── trading-history.html
	├── main.go
	├── README.md
	└── serve.go

如上所示，database目录下为数据库访问层的封装，与最外层的main.go, server.go, client.go构成完整的后端服务，其中server.go负责处理来自前端的各类请求，client.go将处理后的交易请求转发至中央交易系统，main.go负责数据库连接和命令行参数解析。

frontend目录下为完整的前端代码(包含依赖)，采用[Polymer](https://www.polymer-project.org/1.0/)开发。

*请注意go后端部分依赖了[第三方数据库驱动](https://github.com/go-sql-driver/mysql)，拉取项目时使用`go get`可自动拉取第三方依赖包，否则，需要手动下载这些第三方包。*

## 项目构建

请按照参照以下链接安装依赖工具：

### [Git](https://git-scm.com/downloads)

Windows可安装[Github客户端](https://windows.github.com/)，安装后有Git Shell程序可用于日常开发。

Unix/Linux可通过软件包管理工具安装git，如Debian/Ubuntu下：

    apt-get update
    apt-get install git

### [Go](https://golang.org)

+ 请按照[Go官网指导](https://golang.org/doc/install)根据系统下载安装Go
+ 请参照[Go官网指南](https://golang.org/doc/code.html)配置Go开发环境

*注：如因网络原因无法访问[Go官网](https://golang.org), 请使用[国内镜像](http://godoc.golangtc.com)代替。适用于多种系统的Go安装配置指南可以看[这里](https://github.com/astaxie/build-web-application-with-golang/blob/master/zh/01.1.md)； Go国内下载可以看[这里](http://golangtc.com/download)。*

拉取并构建项目：

```shell
	go get -u github.com/fanyang01/stc-server
	# Windows 请根据情况自行调整路径
	cd $GOPATH/src/github.com/fanyang01/stc-server
	# 编译生成名为stc-server(Windows下为stc-server.exe)的可执行文件
	go build
	# 生成用于签署令牌的密钥对
	# Windows可使用预先生成的密钥对，但注意仅作为测试之用
	openssl genrsa -out app.rsa 2048
	openssl rsa -in app.rsa -pubout > app.rsa.pub
```

在项目目录中提供一份JSON格式的数据库配置文件，如db.json：

```json
	{
		"host": "locahost",
		"post": "3306",
		"user": "xxxx",
		"password": "xxxx",
		"dbname": "stock"
	}
```

启动server:

	./stc-server --port=9000


更多选项请运行`./stc-server --help`查看，输出示例：

	Usage of ./stc-server:
	  -cpu=2: number of CPUs to use
	  -db-config="db.json": database config file
	  -host="localhost": host name/address to serve on
	  -port="9000": port listen to
	  -sign-key="app.rsa": private key for sign token
	  -trading-api="http://localhost:8080/api.php": central trading system API address
	  -verify-key="app.rsa.pub": public key for verify token

## TODO

- [x] 修改密码API
- [x] 查询交易历史API
- [ ] <del>股票查询API</del>
- [x] 前端细节

[前端构建](frontend/README.md)
