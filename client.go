package main

import (
	"fmt"
	"net/http"
	"net/url"
)

type tradingReq struct {
	AccountID string        `json:"accountID"`
	Trades    []tradingInst `json:"trades"`
}

type tradingInst struct {
	Type     string  `json:"type"`
	InstID   string  `json:"instID"`
	StockID  string  `json:"stockID"`
	Price    float64 `json:"price"`
	Quantity int64   `json:"quantity"`
}

func submitTradingReq(tr tradingReq) error {
	client := http.DefaultClient
	for _, ti := range tr.Trades {
		v := url.Values{}
		v.Set("account_id", tr.AccountID)

		switch ti.Type {
		case "buy", "sell":
			v.Set("type", "submit")
			v.Set("itype", iType(ti.Type))
			v.Set("price", fmt.Sprintf("%f", ti.Price))
			v.Set("quantity", fmt.Sprintf("%d", ti.Quantity))
			v.Set("sid", ti.StockID)
		case "undo":
			v.Set("type", "withdraw")
			v.Set("iid", ti.InstID)
		default:
			continue
		}
		resp, err := client.PostForm(*tradingAPI, v)
		if err != nil {
			return err
		}
		resp.Body.Close()
	}
	return nil
}

// 3 invalid
// 2 undo
// 1 buy
// 0 sell
func iType(t string) string {
	switch t {
	case "sell":
		return "0"
	case "buy":
		return "1"
	case "undo":
		return "2"
	default:
		return "3"
	}
}

// 1 finish
// 0 processing
// -1 undo seccess
// -2 deleted
