package database

import "time"

// Stock contains all information about a stock
type Stock struct {
	ID          string  `json:"id"`
	Name        string  `json:"name"`
	Price       float64 `json:"price"`
	State       int     `json:"state"`
	UpLimit     float64 `json:"upLimit"`
	DownLimit   float64 `json:"downLimit"`
	Description string  `json:"description"`
	NetAssets   float64 `json:"netAssets"`
	Eps         float64 `json:"eps"`
	Shares      int64   `json:"shares"`
	IndustryID  string  `json:"industryID"`
}

// StockInstant is the fresh data of a stock
type StockInstant struct {
	Price      float64   `json:"price"`
	Change     float64   `json:"change"`
	ChangeRate float64   `json:"changeRate"`
	Time       time.Time `json:"time"`
	MaxBuy     float64   `json:"maxBuy"`
	MinSell    float64   `json:"minSell"`
	Amount     int64     `json:"amount"`
	Volume     int64     `json:"volume"`
	Sum        float64   `json:"sum"`
}

// PriceRange is the price range during different period
type PriceRange struct {
	OpenPrice float64 `json:"openPrice"`
	DayLow    float64 `json:"dayLow"`
	DayHigh   float64 `json:"dayHigh"`
	WeekLow   float64 `json:"weekLow"`
	WeekHigh  float64 `json:"weekHigh"`
	MonthLow  float64 `json:"monthLow"`
	MonthHigh float64 `json:"monthHigh"`
}

// HoldingStock is a stock that an account holds
type HoldingStock struct {
	ID           string  `json:"id"`
	Name         string  `json:"name"`
	Shares       int64   `json:"shares"`
	HoldingPrice float64 `json:"holdingPrice"`
	Price        float64 `json:"price"`
	Profit       float64 `json:"profit"`
}

// Account holds capital status and holding stocks infomation
type Account struct {
	ID            string         `json:"id"`
	SecurityID    string         `json:"sid"`
	PresentCap    float64        `json:"presentCap"`
	FrozenCap     float64        `json:"frozenCap"`
	UsableCap     float64        `json:"usableCap"`
	Profit        float64        `json:"profit"`
	HoldingStocks []HoldingStock `json:"holdingStocks"`
}

// Instruction is a trading instruction
type Instrucion struct {
	ID         string  `json:"id"`
	SecurityID string  `json:"securityID"`
	AccountID  string  `json:"accountID"`
	Type       int     `json:"type"`
	StockID    string  `json:"stockID"`
	State      int     `json:"state"`
	Price      float64 `json:"price"`
	Quantity   int64   `json:"quantity"`
}

// Individual represents a individual security user
type Individual struct {
	SecurityID   string
	Identity     string
	Name         string
	Sex          string
	Address      string
	Mobile       string
	Company      string
	Career       string
	Edu          string
	DelegateName string
	DelegateID   string
}

// Article is news about a specific industry
type Article struct {
	ID       string    `json:"id"`
	Title    string    `json:"title"`
	Author   string    `json:"author"`
	SubmitID string    `json:"submitID"`
	Content  string    `json:"content"`
	CatID    string    `json:"catID"`
	Time     time.Time `json:"time"`
}

// Industry is ...
type Industry struct {
	ID          string    `json:"id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Articles    []Article `json:"articles"`
}
