package database

func (db *DB) UpdatePassword(pw, id string) error {
	uPasswordSQL := `UPDATE account SET signpasswd = ? WHERE account_id = ?`

	tx, err := db.db.Begin()
	if err != nil {
		return err
	}
	stmt, err := tx.Prepare(uPasswordSQL)
	if err != nil {
		tx.Rollback()
		return err
	}
	_, err = stmt.Exec(pw, id)
	if err != nil {
		tx.Rollback()
		return err
	}
	stmt.Close()
	return tx.Commit()
}
