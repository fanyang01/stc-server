package database

import "database/sql"

type DB struct {
	db *sql.DB
}

// New try to connect to database and return a new connection
func New(db *sql.DB) (*DB, error) {
	if err := db.Ping(); err != nil {
		return nil, err
	}
	return &DB{db: db}, nil
}

// Close closes the database connection
func (db *DB) Close() {
	db.db.Close()
}
