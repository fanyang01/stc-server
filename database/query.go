package database

import "database/sql"

func (db *DB) QueryStock(id string) (*Stock, error) {
	qStockSQL := `SELECT sid, sname, sprice, state, uplimit, downlimit, description, netassets, eps, total_shares, industry_id FROM stock WHERE sid = ?`
	var st Stock
	err := db.db.QueryRow(qStockSQL, id).Scan(
		&st.ID,
		&st.Name,
		&st.Price,
		&st.State,
		&st.UpLimit,
		&st.DownLimit,
		&st.Description,
		&st.NetAssets,
		&st.Eps,
		&st.Shares,
		&st.IndustryID,
	)
	if err != nil {
		return nil, err
	}
	return &st, nil
}

func (db *DB) QueryPassword(id string) (string, error) {
	qPasswordSQL := `SELECT signpasswd FROM account WHERE account_id = ?`

	var pw string
	err := db.db.QueryRow(qPasswordSQL, id).Scan(&pw)
	return pw, err
}

func (db *DB) QueryAccount(id string) (*Account, error) {
	qAccountSQL := `SELECT account_id, presentcap, usablecap, frozencap, profit FROM account WHERE account_id = ?`
	qSecurityIDSQL := `SELECT id FROM connect WHERE account_id = ?`
	qHoldingStocksSQL := `SELECT A.sid, S.sname, S.sprice, A.shares, A.price FROM account_stock AS A, stock AS S WHERE A.account_id = ? AND S.sid = A.sid`
	qStockProfitSQL := `SELECT SUM(shares) * (SELECT price FROM stock WHERE sid = ?) - SUM(price * shares) FROM account_stock WHERE sid = ? AND account_id = ?`

	var a Account
	err := db.db.QueryRow(qAccountSQL, id).Scan(
		&a.ID,
		&a.PresentCap,
		&a.UsableCap,
		&a.FrozenCap,
		&a.Profit,
	)
	if err != nil {
		return nil, err
	}
	// Query Security ID
	err = db.db.QueryRow(qSecurityIDSQL, id).Scan(&a.SecurityID)
	switch {
	case err == sql.ErrNoRows:
	case err != nil:
		return nil, err
	default:
	}
	// Query holding stocks
	rows, err := db.db.Query(qHoldingStocksSQL, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var h HoldingStock
		err := rows.Scan(
			&h.ID,
			&h.Name,
			&h.Price,
			&h.Shares,
			&h.HoldingPrice,
		)
		if err != nil {
			return nil, err
		}
		a.HoldingStocks = append(a.HoldingStocks, h)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	// Query holding stocks profit
	stmt, err := db.db.Prepare(qStockProfitSQL)
	defer stmt.Close()
	if err != nil {
		return nil, err
	}
	for _, s := range a.HoldingStocks {
		err := stmt.QueryRow(s.ID, s.ID, a.ID).Scan(&a.Profit)
		if err != nil {
			return nil, err
		}
	}
	return &a, nil
}

// QueryHistoryBySid queries trading history of specified id
func (db *DB) QueryHistory(id string) ([]Instrucion, error) {
	qHistorySQL := `SELECT iid, itype, sid, price, quantity, state FROM instruct WHERE account_id = ?`

	var its []Instrucion
	rows, err := db.db.Query(qHistorySQL, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var it Instrucion
		err := rows.Scan(
			&it.ID,
			&it.Type,
			&it.StockID,
			&it.Price,
			&it.Quantity,
			&it.State,
		)
		if err != nil {
			return nil, err
		}
		its = append(its, it)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return its, nil
}

func (db *DB) QueryIndustry(id string) (*Industry, error) {
	qIndustrySQL := `SELECT name, description FROM industry WHERE id = ?`
	qIndustryArticlesSQL := `SELECT id, title, author, submit_id, content, time FROM industry_article
	WHERE cat_id = ? ORDER BY time DESC LIMIT 10`

	var ids Industry
	err := db.db.QueryRow(qIndustrySQL, id).Scan(
		&ids.Name,
		&ids.Description,
	)
	if err != nil {
		return nil, err
	}
	// Query articles
	rows, err := db.db.Query(qIndustryArticlesSQL, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var a Article
		err := rows.Scan(
			&a.ID,
			&a.Title,
			&a.Author,
			&a.SubmitID,
			&a.Content,
			&a.Time,
		)
		if err != nil {
			return nil, err
		}
		ids.Articles = append(ids.Articles, a)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return &ids, nil
}

func (db *DB) QueryStockInstant(id string) (*StockInstant, error) {
	qStockInstantSQL := `SELECT price, time, max_buy_price, min_sell_price, amount, sumtotal, vol FROM interday
	WHERE sid = ? ORDER BY time DESC LIMIT 1`
	qOpenPriceSQL := `SELECT open_price FROM day_price_record WHERE date = CURDATE() AND sid = ?`

	var si StockInstant
	err := db.db.QueryRow(qStockInstantSQL, id).Scan(
		&si.Price,
		&si.Time,
		&si.MaxBuy,
		&si.MinSell,
		&si.Amount,
		&si.Sum,
		&si.Volume,
	)
	if err != nil {
		return nil, err
	}
	// Query open price
	var openPrice float64
	err = db.db.QueryRow(qOpenPriceSQL, id).Scan(&openPrice)
	switch {
	case err == sql.ErrNoRows:
	case err != nil:
		return nil, err
	default:
		si.Change = si.Price - openPrice
		si.ChangeRate = si.Change / openPrice
	}
	return &si, nil
}

func (db *DB) QueryPriceRange(id string) (*PriceRange, error) {
	qPriceDayRangeSQL := `SELECT open_price, dayhigh, daylow FROM day_price_record WHERE date = CURDATE() AND sid = ?`
	qPriceWeekRangeSQL := `SELECT weekhigh, weeklow FROM week_price_record WHERE WEEK(date) = WEEK(CURDATE()) AND sid = ?`
	qPriceMonthRangeSQL := `SELECT monthhigh, monthlow FROM month_price_record WHERE MONTH(date) = MONTH(CURDATE()) AND sid = ?`

	var pr PriceRange
	err := db.db.QueryRow(qPriceDayRangeSQL, id).Scan(
		&pr.OpenPrice,
		&pr.DayLow,
		&pr.DayHigh,
	)
	if err != nil {
		return nil, err
	}
	err = db.db.QueryRow(qPriceWeekRangeSQL, id).Scan(
		&pr.WeekLow,
		&pr.WeekHigh,
	)
	if err != nil {
		return nil, err
	}
	err = db.db.QueryRow(qPriceMonthRangeSQL, id).Scan(
		&pr.MonthLow,
		&pr.MonthHigh,
	)
	if err != nil {
		return nil, err
	}
	return &pr, nil
}

func (db *DB) QueryPrice(id string) (float64, error) {
	qPriceSQL := `SELECT price FROM interday WHERE sid = ? ORDER BY time DESC LIMIT 1`

	var price float64
	err := db.db.QueryRow(qPriceSQL, id).Scan(&price)
	return price, err
}
